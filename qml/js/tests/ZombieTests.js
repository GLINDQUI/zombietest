function init(platform) {
    var Promise;

    (function init() {
        Promise = platform.getPromise();
    })();

    function _randomPromise() {
        var random = Math.random(666);

        if (random <= 0.0001) {
            throw("b0rk!");
        }

        return random > 0.5 ? Promise.resolve() : Promise.reject();
    }

    function testPromise() {
        _randomPromise().then(
            function() { valuesModel.promises++; },
            function() { valuesModel.promises--; }
        );
    }

    function testPromiseTryCatch() {
        try {
            _randomPromise().then(
                function() { valuesModel.promises++; },
                function() { valuesModel.promises--; }
            );
        } catch(e) {
            exceptionModel.promiseException = "Lie.Promise test @ " + valuesModel.networkCalls + ": " + JSON.stringify(e);
            valuesModel.promises = valuesModel.networkCalls;
        }
    }

    function testRequestDestination() {
        if (testModesModel.useTryCatch) {
            testComplexMathTryCatch();
        } else {
            testComplexMath();
        }

        Navigation.requestDestinationInformation();
        Navigation.requestItineraryInformation();

        valuesModel.destinationRequestedCount++;
    }

    function _getMarket() {
        return platform.network.sendRequest({
            method: "GET",
            feature: "spautils",
            uri: "utils/getmarket",
            responseType: "text",
            silent: true
         });
    }

    function getDataPromise() {
        return _getMarket();
    }

    function testRemainder() {
        valuesModel.remainderValue = statusModel.now % (60 * 1000);
    }

    function testRemainderTryCatch() {
        try {
            valuesModel.remainderValue = statusModel.now % (60 * 1000);
        } catch(e) {
            exceptionModel.generalException = "remainder test @ " + valuesModel.networkCalls + ": " + JSON.stringify(e);
            console.log("ZombieTests.testRemainderTryCatch: caught exception");
        }
    }

    function testMathCeil() {
        valuesModel.mathCeilValue = Math.ceil(statusModel.now / (60 * 1000));
    }

    function testMathCeilTryCatch() {
        try {
            valuesModel.mathCeilValue = Math.ceil(statusModel.now / (60 * 1000));
        } catch(e) {
            exceptionModel.generalException = "Math.ceil test @ " + valuesModel.networkCalls + ": " + JSON.stringify(e);
            console.log("ZombieTests.testMathCeilTryCatch: caught exception");
        }
    }

    function testMathFloor() {
        valuesModel.mathFloorValue = Math.floor(statusModel.now / (60 * 1000))
    }

    function testMathFloorTryCatch() {
        try{
            valuesModel.mathFloorValue = Math.floor(statusModel.now / (60 * 1000))
        } catch(e) {
            exceptionModel.generalException = "Math.floor test @ " + valuesModel.networkCalls + ": " + JSON.stringify(e);
            console.log("ZombieTests.testMathFloorTryCatch: caught exception");
        }
    }

    function testComplexMath() {
        /*
        var value = 0;
        value = statusModel.interval / (60 * 10);
        value = Math.ceil(statusModel.interval / (60 * 10));
        value = Math.ceil(statusModel.interval / (60 * 10)) % 6;
        value = Math.floor((Math.ceil(statusModel.interval / (60 * 10)) % 60) / 10);
        */

        valuesModel.combinedMathValue = ++valuesModel.calculations + ": " + Math.ceil(statusModel.interval / (60 * 10)) % 6;
    }

    function testComplexMathTryCatch() {
        try{
            /*
            var value = 0;
            value = statusModel.interval / (60 * 10);
            value = Math.ceil(statusModel.interval / (60 * 10));
            value = Math.ceil(statusModel.interval / (60 * 10)) % 6;
            value = Math.floor((Math.ceil(statusModel.interval / (60 * 10)) % 60) / 10);
            */

            valuesModel.combinedMathValue = ++valuesModel.calculations + ": " + Math.ceil(statusModel.interval / (60 * 10)) % 6;
        } catch(e) {
            exceptionModel.generalException = "complex math test @ " + valuesModel.networkCalls + ": " + JSON.stringify(e);
            console.log("ZombieTests.textComplexMathTryCatch: caught exception");
        }
    }

    return {
        // Public functions
        init: init,
        testPromise: testPromise,
        testPromiseTryCatch: testPromiseTryCatch,
        getDataPromise: getDataPromise,
        testRemainder: testRemainder,
        testRemainderTryCatch: testRemainderTryCatch,
        testMathCeil: testMathCeil,
        testMathCeilTryCatch: testMathCeilTryCatch,
        testMathFloor: testMathFloor,
        testMathFloorTryCatch: testMathFloorTryCatch,
        testComplexMath: testComplexMath,
        testComplexMathTryCatch: testComplexMathTryCatch,
        testRequestDestination: testRequestDestination
    };
}
