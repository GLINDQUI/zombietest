function init() {
    (function init() {
    })();

    function startTests() {
        testModesModel.runRemainderTest = true;
        testModesModel.runMathCeilTest = true;
        testModesModel.runMathFloorTest = true;
        testModesModel.runCombinedMathTest = true;
        testModesModel.runPromiseTest = true;
    }

    function toggleTryCatch() {
        testModesModel.useTryCatch = !testModesModel.useTryCatch;
        valuesModel.promises = 0;
        valuesModel.calculations = 0;
    }

    function toggleGetMarket() {
        testModesModel.doGetMarket = !testModesModel.doGetMarket;

        if (testModesModel.doGetMarket) {
            getDataTimer.start();
            statusModel.networkStartTime = (new Date()).getTime();
            valuesModel.networkCalls = 0;
        }
    }

    function onDestinationUpdated() {
        valuesModel.destinationUpdatedCount++;
    }

    return {
        startTests: startTests,
        toggleTryCatch: toggleTryCatch,
        toggleGetMarket: toggleGetMarket,
        onDestinationUpdated: onDestinationUpdated
    };
}
