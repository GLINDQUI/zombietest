// version 0x10016
/* Copyright (c) 2015 Volvo Car Corporation
 * All rights reserved.
 */

import QtQuick 2.0
import SpaComponents 1.0
import "views"
import "js/tests/ZombieTests.js" as ZombieTests
import "js/controllers/viewControllers/MaximizedViewController.js" as MaximizedViewController

SpaApplicationArea {
    id: appContainer

    /////////////////////////////////////////////////////
    /////////////////////// Views ///////////////////////
    /////////////////////////////////////////////////////

    MaximizedView {
        id: maximizedView
    }

    /////////////////////////////////////////////////////
    ////////////////// View Controllers /////////////////
    /////////////////////////////////////////////////////

    property var maximizedViewController: null
    property var zombieTests: null

    /////////////////////////////////////////////////////
    /////////////////// View Models /////////////////////
    /////////////////////////////////////////////////////

    QtObject {
        id: testModesModel

        property bool useTryCatch: true
        property bool doGetMarket: false

        property bool runRemainderTest: false
        property bool runMathCeilTest: false
        property bool runMathFloorTest: false
        property bool runCombinedMathTest: false
        property bool runPromiseTest: false
        property bool runRequestDestinationTest: false
        property bool runTimestampTest: false
    }

    QtObject {
        id: statusModel

        property double startTime: { return (new Date()).getTime(); }
        property double networkStartTime: 0
        property double now: 0
        property double interval: 0
        property double since: 0
    }

    QtObject{
        id: valuesModel

        property string remainderValue: ""
        property string mathCeilValue: ""
        property string mathFloorValue: ""
        property string combinedMathValue: ""

        property int networkCalls: 0
        property int promises: 0
        property int calculations: 0

        property int destinationRequestedCount: 0
        property int destinationUpdatedCount: 0

        property double timestamp: 0
    }

    QtObject {
        id: exceptionModel

        property string generalException: ""
        property string promiseException: ""
        property string getDataException: ""
    }

    /////////////////////////////////////////////////////
    ////////////////////// Timers ///////////////////////
    /////////////////////////////////////////////////////

    Timer {
        id: tickTimer

        interval: 60 * 1000
        repeat: true
        triggeredOnStart: true

        onTriggered: {
            Viewer.setSubText((!!versionString ? versionString : "Zombiecalypse") + qsTr(" running for %1 min").arg(statusModel.since));
        }
    }

    Timer {
        id: getDataTimer

        interval: 1
        repeat: false

        onTriggered: {
            try {
                if (valuesModel.networkCalls >= 10000000000) {
                    valuesModel.networkCalls = 0;
                    statusModel.networkStartTime = (new Date()).getTime();
                }

                statusModel.now = (new Date()).getTime();
                zombieTests.getDataPromise()
                    .then(function(data) {
                        var interval = statusModel.now - statusModel.networkStartTime;
                        valuesModel.networkCalls++;
                        Viewer.setMainText(valuesModel.networkCalls + " (" + Math.floor(valuesModel.networkCalls / (interval / 100000)) / 100 + " req/s)");
                        if (testModesModel.doGetMarket) {
                            getDataTimer.start();
                        }
                    }, function(error) {
                        var interval = statusModel.now - statusModel.networkStartTime;
                        Viewer.setMainText(valuesModel.networkCalls + " (" + Math.floor(valuesModel.networkCalls / (interval / 100000)) / 100 + " req/s)");
                        exceptionModel.getDataException = "getDataTimer: getDataPromise failed @" + valuesModel.networkCalls + ": " + JSON.stringify(error);
                        if (testModesModel.doGetMarket) {
                            getDataTimer.start();
                        }
                    })
                ;
            } catch(e) {
                exceptionModel.getDataException = "getDataTimer @ " + valuesModel.networkCalls + ": " + JSON.stringify(e);
            }
        }
    }

    Timer {
        id: testTimer

        interval: 100
        repeat: true
        triggeredOnStart: true

        onTriggered: {
            statusModel.now = (new Date()).getTime();
            statusModel.interval = statusModel.now - statusModel.startTime;

            try {
                statusModel.since = Math.ceil(statusModel.interval / (60 * 1000));
            } catch(e) {
                console.log("main.testTimer: exception caught");
            }

            if (testModesModel.runPromiseTest) {
                if (testModesModel.useTryCatch) {
                    zombieTests.testPromiseTryCatch();
                } else {
                    zombieTests.testPromise();
                }
            } else {
                valuesModel.remainderValue = "";
            }

            if (testModesModel.runRemainderTest) {
                if (testModesModel.useTryCatch) {
                    zombieTests.testRemainderTryCatch();
                } else {
                    zombieTests.testRemainder();
                }
            } else {
                valuesModel.remainderValue = "";
            }

            if (testModesModel.runMathCeilTest) {
                if (testModesModel.useTryCatch) {
                    zombieTests.testMathCeilTryCatch();
                } else {
                    zombieTests.testMathCeil();
                }
            } else {
                valuesModel.mathCeilValue = "";
            }

            if (testModesModel.runMathFloorTest) {
                if (testModesModel.useTryCatch) {
                    zombieTests.testMathFloorTryCatch();
                } else {
                    zombieTests.testMathFloor();
                }
            } else {
                valuesModel.mathFloorValue = "";
            }

            if (testModesModel.runCombinedMathTest) {
                if (testModesModel.useTryCatch) {
                    zombieTests.testComplexMathTryCatch();
                } else {
                    zombieTests.testComplexMath();
                }
            } else {
                if (!testModesModel.runRequestDestinationTest) { valuesModel.combinedMathValue = ""; }
            }

            if (testModesModel.runTimestampTest) {
                valuesModel.timestamp = statusModel.now;
            }
        }
    }

    Timer {
        id: testSignalsTimer

        interval: 1000
        repeat: true
        triggeredOnStart: true

        onTriggered: {
            if (testModesModel.runRequestDestinationTest) {
                zombieTests.testRequestDestination();
            }
        }
    }

    /////////////////////////////////////////////////////
    /////////////////// Init ////////////////////////////
    /////////////////////////////////////////////////////

    Component.onCompleted: {
        // platform and framwork stuff
        var scorfa = Scorfa.scorfa;
        scorfa.fixLogging(Log);

        var platform = SpaPlatform.platform;
        platform.init(scorfa, Lie.Promise);

        Navigation.setApiEnabled();

        // controllers and js files
        maximizedViewController = MaximizedViewController.init();
        zombieTests = ZombieTests.init(platform);

        // pretty title
        Viewer.setMainText("");
        Viewer.setSubText((!!versionString ? versionString : "Zombiecalypse NOW"));

        // init values and start timers
        testSignalsTimer.start();
        testTimer.start();
        tickTimer.start();
    }
}
