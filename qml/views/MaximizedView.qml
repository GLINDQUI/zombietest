import QtQuick 2.0
import SpaComponents 1.0

SpaMaximizedView {
    id: maximizedView
    visible: Viewer.renderState === SpaApplicationViewer.RenderStateExpanded

    Item {
        y: 160

        SpaHomescreenToolbar {
            id: maximizedViewToolbar

            x: 10
            y: 20

            textButtonCount: 3

            buttonText1: qsTr("Start all tests");
            buttonEnabled1: true
            onButton1Clicked: {
                maximizedViewController.startTests();
            }

            buttonText2: qsTr("Cloud Test is %1").arg(testModesModel.doGetMarket ? "ON" : "OFF");
            buttonEnabled2: true
            onButton2Clicked: {
                maximizedViewController.toggleGetMarket();
            }

            buttonText3: qsTr("Try/Catch is %1").arg(testModesModel.useTryCatch ? "ON" : "OFF");
            buttonEnabled3: true
            onButton3Clicked: {
                maximizedViewController.toggleTryCatch();
            }
        }

        Flickable {
            anchors { left: maximizedViewToolbar.right; top: maximizedViewToolbar.top; leftMargin: 15; }

            width: 500
            contentWidth: 500

            height: 390
            contentHeight: testOptions.height

            clip: true
            flickableDirection: Flickable.VerticalFlick

            Column {
                id: testOptions

                spacing: 1

                SpaNarrowListItem {
                    itemType: "01.00.01"

                    leftText: "Remainder (%) test"
                    leftSubtext: valuesModel.remainderValue
                    rightThemeIcon: testModesModel.runRemainderTest ? "checkbox1.png" : "checkbox0.png"

                    clickHighlight: false
                    actionArea: true
                    actionHighlight: true

                    onActionClicked: {
                        testModesModel.runRemainderTest = !testModesModel.runRemainderTest;
                    }
                }

                SpaNarrowListItem {
                    itemType: "01.00.01"

                    leftText: "Math.Ceil() test"
                    leftSubtext: valuesModel.mathCeilValue
                    rightThemeIcon: testModesModel.runMathCeilTest ? "checkbox1.png" : "checkbox0.png"

                    clickHighlight: false
                    actionArea: true
                    actionHighlight: true

                    onActionClicked: {
                        testModesModel.runMathCeilTest = !testModesModel.runMathCeilTest;
                    }
                }

                SpaNarrowListItem {
                    itemType: "01.00.01"

                    leftText: "Math.Floor() test"
                    leftSubtext: valuesModel.mathFloorValue
                    rightThemeIcon: testModesModel.runMathFloorTest ? "checkbox1.png" : "checkbox0.png"

                    clickHighlight: false
                    actionArea: true
                    actionHighlight: true

                    onActionClicked: {
                        testModesModel.runMathFloorTest = !testModesModel.runMathFloorTest;
                    }
                }

                SpaNarrowListItem {
                    itemType: "01.00.01"

                    leftText: "Do ALL the math"
                    leftSubtext: valuesModel.combinedMathValue
                    rightThemeIcon: testModesModel.runCombinedMathTest ? "checkbox1.png" : "checkbox0.png"

                    clickHighlight: false
                    actionArea: true
                    actionHighlight: true

                    onActionClicked: {
                        testModesModel.runCombinedMathTest = !testModesModel.runCombinedMathTest;
                    }
                }

                SpaNarrowListItem {
                    itemType: "01.00.01"

                    leftText: "Lie.Promise test"
                    leftSubtext: valuesModel.promises
                    rightThemeIcon: testModesModel.runPromiseTest ? "checkbox1.png" : "checkbox0.png"

                    clickHighlight: false
                    actionArea: true
                    actionHighlight: true

                    onActionClicked: {
                        testModesModel.runPromiseTest = !testModesModel.runPromiseTest;
                    }
                }

                SpaNarrowListItem {
                    itemType: "01.00.01"

                    leftText: "requestDestination() test"
                    leftSubtext: "sent: " + valuesModel.destinationRequestedCount + ", received: " + valuesModel.destinationUpdatedCount
                    rightThemeIcon: testModesModel.runRequestDestinationTest ? "checkbox1.png" : "checkbox0.png"

                    clickHighlight: false
                    actionArea: true
                    actionHighlight: true

                    onActionClicked: {
                        testModesModel.runRequestDestinationTest = !testModesModel.runRequestDestinationTest;
                        if (testModesModel.runRequestDestinationTest) {
                            Navigation.destinationUpdated.connect(maximizedViewController.onDestinationUpdated);
                        } else {
                            Navigation.destinationUpdated.disconnect(maximizedViewController.onDestinationUpdated);
                        }
                    }
                }

                SpaNarrowListItem {
                    itemType: "01.00.01"

                    leftText: "timestamp: " + valuesModel.timestamp
                    leftSubtext: (new Date(valuesModel.timestamp)).toUTCString()
                    rightThemeIcon: testModesModel.runTimestampTest ? "checkbox1.png" : "checkbox0.png"

                    clickHighlight: false
                    actionArea: true
                    actionHighlight: true

                    onActionClicked: {
                        testModesModel.runTimestampTest = !testModesModel.runTimestampTest;
                    }
                }

                SpaLabel {
                    height: 42
                    width: parent.width

                    visible: !!exceptionModel.generalException
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    wrapMode : Text.WordWrap
                    fontClass: "InfoText"

                    text: "caught: " + exceptionModel.generalException;
                }

                SpaLabel {
                    height: 42
                    width: parent.width

                    visible: !!exceptionModel.promiseException
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    wrapMode : Text.WordWrap
                    fontClass: "InfoText"

                    text: "caught: " + exceptionModel.promiseException;
                }

                SpaLabel {
                    height: 42
                    width: parent.width

                    visible: !!exceptionModel.getDataException
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    wrapMode : Text.WordWrap
                    fontClass: "InfoText"

                    text: "caught: " + exceptionModel.getDataException;
                }
            }
        }
    }
}
