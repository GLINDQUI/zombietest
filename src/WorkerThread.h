#ifndef WORKERTHREAD_H
#define WORKERTHREAD_H

#include <QThread>

class WorkerThread : public QThread
{
private:
    void run();

public:
    WorkerThread();
};

#endif // WORKERTHREAD_H
