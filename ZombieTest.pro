TEMPLATE = app

VERSION     = $$system(type version)
VERSTR      = '\\"$${VERSION}\\"'

DEFINES     += VER=\"$${VERSTR}\"

QT          += quick network
HEADERS     += src/WorkerThread.h
SOURCES     += main.cpp src/WorkerThread.cpp

qmlsources.source   = qml
qmlsources.target   = .
DEPLOYMENTFOLDERS   += qmlsources

include (../SpaApplicationFramework/src/SpaApplicationFramework.pri)

OTHER_FILES += \
    qml/main.qml \
    ManifestFile.ini \
    qml/views/*.* \
    qml/js/controllers/viewControllers/*.* \
    qml/js/tests/*.*

applicationDeployment()
