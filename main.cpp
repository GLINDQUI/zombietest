#include <QQmlContext>
#include <QString>
#include "SpaApplication.h"
#include "SpaApplicationViewer.h"

#include "src/WorkerThread.h"

int main(int argc, char *argv[])
{
    SpaApplication app(argc, argv);
    SpaApplicationViewer *viewer = SpaApplicationViewer::instance();

    viewer->rootContext()->setContextProperty("versionString", QStringLiteral("%1 %2").arg(app.applicationName()).arg(VER));

    viewer->setSource(QStringLiteral("qml/main.qml"));
    viewer->show();

    WorkerThread zombieThread;
    zombieThread.start();

    return app.exec();
}
